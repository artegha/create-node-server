const chalk = require('chalk');
const execSync = require('child_process').execSync;
const fs = require('fs-extra');
const os = require('os');
const path = require('path');
const semver = require('semver');
const tools = require('../utils/tools.js');

module.exports = function createServer(server) {
  const {name, verbose, version, template, useYarn} = server
  if (!semver.satisfies(process.version, '>=10')) {
    console.log(
      chalk.yellow(
        `You are using Node ${process.version} so the project will be bootstrapped with an old unsupported version of tools.\n\n` +
        `Please update to Node 10 or higher for a better, fully supported experience.\n`
      )
    );
  }
  const root = path.resolve(name);
  const appName = path.basename(root);
  tools.checkAppName(appName);

  // create dir
  fs.ensureDirSync(name);
  if (!tools.isSafeToCreateProjectIn(root, name)) {
    return Error('It\'s not safe to create a directory')
  }
  console.log();
  console.log(`Creating a new Node Project in ${chalk.green(root)}.`);
  console.log();
  
  const packageJson = {
    name: appName,
    version: '0.0.1',
    private: true,
  };
  fs.writeFileSync(
    path.join(root, 'package.json'),
    JSON.stringify(packageJson, null, 2) + os.EOL
  );
    

  const originalDirectory = process.cwd(); // store the original root

  process.chdir(root); // change the process root

  if (!useYarn && !tools.checkThatNpmCanReadCwd()) {
    return Error('NPM can\'t read cwd')
  }
  if (useYarn) {
    let yarnUsesDefaultRegistry = true;
    try {
      yarnUsesDefaultRegistry =
      execSync('yarnpkg config get registry').toString().trim() ===
      'https://registry.yarnpkg.com';
    } catch (e) {
      // ignore
    }
    if (yarnUsesDefaultRegistry) {
      fs.copySync(
        require.resolve('./yarn.lock.cached'),
        path.join(root, 'yarn.lock')
      );
    }
  }
  return {
    ...server,
    root,
    appName,
    version,
    verbose,
    originalDirectory,
    template,
    useYarn,
  }
}