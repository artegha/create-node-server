const chalk = require('chalk');
const semver = require('semver');
const tools = require('../utils/tools.js')

module.exports = async function run(server){
  const {
    root,
    appName,
    version,
    verbose,
    originalDirectory,
    template,
    useYarn
  } = server
  const packageToInstall = await tools.getInstallPackage(version, originalDirectory)
  const templateToInstall = await tools.getTemplateInstallPackage(template, originalDirectory)
  const allDependencies = [];
    
  console.log('Installing packages. This might take a couple of secondes.');

  const packageInfo = await tools.getPackageInfo(packageToInstall)
  const templateInfo = await tools.getPackageInfo(templateToInstall)

  const isOnline = await  tools.checkIfOnline(useYarn)
  
  let packageVersion = semver.coerce(undefined);
  const templatesVersionMinimum = '0.0.3';
      
  // Assume compatibility if we can't test the version.
    if (!semver.valid(packageVersion)) {
      packageVersion = templatesVersionMinimum;
  }
    
  // Only support templates when used alongside new react-scripts versions.
  const supportsTemplates = semver.gte(
    packageVersion,
    templatesVersionMinimum
  );
  if (supportsTemplates) {
    allDependencies.push(templateToInstall);
  } else if (template) {
    console.log('');
    console.log(
      `The ${chalk.cyan(packageInfo.name)} version you're using ${
        packageInfo.name === 'react-scripts' ? 'is not' : 'may not be'
      } compatible with the ${chalk.cyan('--template')} option.`
      );
    console.log('');
  }
        
  console.log(
    `Installing ${chalk.cyan(packageInfo.name)}${
        supportsTemplates ? ` with ${chalk.cyan(templateInfo.name)}` : ''
      }...`
  );
  console.log();

  
  const packageName = packageInfo.name;
  const templateName = templateInfo.name;
  // checkNodeVersion(packageName);
  // setCaretRangeForRuntimeDeps(packageName);

  // if (version === '@0.9.x') {
  //   console.log(
  //     chalk.yellow(
  //       `\nNote: the project was bootstrapped with an old unsupported version of tools.\n` +
  //         `Please update to Node >=10 and npm >=6 to get supported tools in new projects.\n`
  //     )
  //   );
  // }
  const dependencies = allDependencies
  return { ...server, packageName, templateName, root, useYarn, dependencies, verbose, isOnline}
}
