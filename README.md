# Create Node Server <img alt="npm (scoped)" src="https://img.shields.io/npm/v/@artegha/create-node-server">
Create modern Node.js apps with no build configuration. In a single command, this tool bootstraps a Typescript + Express. Tottaly inspired by and based on [create-react-app](https://github.com/facebook/create-react-app).

<!-- ![Terminal Example](docs/term1.svg) -->

## Quick start

#### Creating a Server

    npx @artegha/create-node-server my-server
    cd my-server
    npm start

_Creates a new repository and starts an Express backend._
#### Deploying to Production

    npm run prod

_Build you server for production_

## What’s Included?

`create-node-server` automatically sets up and manages:

- Developer tools: ESLint, editorconfig, prettier, automatic reload with Nodemon and Typescript 

## Contributing

- If you feel that this tool can be improved, feel free to open an issue or pull request!
- You can also create template like with create-react-app