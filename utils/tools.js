const chalk = require('chalk');
const dns = require('dns');
const execSync = require('child_process').execSync;
const fs = require('fs-extra');
const os = require('os');
const path = require('path');
const semver = require('semver');
const spawn = require('cross-spawn');
const url = require('url');
const validateProjectName = require('validate-npm-package-name');

module.exports = {
   getInstallPackage: function(version, originalDirectory) {
  let packageToInstall = ''; // create-node-server
  const validSemver = semver.valid(version);
  if (validSemver) {
    packageToInstall += `@${validSemver}`;
  } else if (version) {
    if (version[0] === '@' && !version.includes('/')) {
      packageToInstall += version;
    } else if (version.match(/^file:/)) {
      packageToInstall = `file:${path.resolve(
        originalDirectory,
        version.match(/^file:(.*)?$/)[1]
      )}`;
    } else {
      // for tar.gz or alternative paths
      packageToInstall = version;
    }
  }

  return Promise.resolve(packageToInstall);
  },
  getTemplateInstallPackage: function(template, originalDirectory) {
    let templateToInstall = 'cns-template-typescript-express';
    // console.log(template)
    if (template) {
      if (template.match(/^file:/)) {
        templateToInstall = `file:${path.resolve(
          originalDirectory,
          template.match(/^file:(.*)?$/)[1]
        )}`;
      } else if (
        template.includes('://') ||
        template.match(/^.+\.(tgz|tar\.gz)$/)
      ) {
        // for tar.gz or alternative paths
        templateToInstall = template;
      } else {
        // Add prefix 'cns-template-' to non-prefixed templates, leaving any
        // @scope/ intact.
        const packageMatch = template.match(/^(@[^/]+\/)?(.+)$/);
        const scope = packageMatch[1] || '';
        const templateName = packageMatch[2];
        // console.log(templateName)
        // console.log(templateToInstall)
        if (
          templateName == templateToInstall ||
          templateName.startsWith(`${templateToInstall}-`)
        ) {
          // Covers:
          // - cns-template
          // - @SCOPE/cns-template
          // - cns-template-NAME
          // - @SCOPE/cns-template-NAME
          templateToInstall = `${scope}${templateName}`;
        } else if (templateName.startsWith('@')) {
          // Covers using @SCOPE only
          templateToInstall = `${templateName}/${templateToInstall}`;
        } else {
          // Covers templates without the `cns-template` prefix:
          // - NAME
          // - @SCOPE/NAME
          templateToInstall = `${scope}${templateToInstall}-${templateName}`;
        }
      }
    }
    return Promise.resolve(templateToInstall);
  },

  getPackageInfo: function(installPackage) {
    if (installPackage.match(/^.+\.(tgz|tar\.gz)$/)) {
      return getTemporaryDirectory()
        .then(obj => {
          let stream;
          if (/^http/.test(installPackage)) {
            stream = hyperquest(installPackage);
          } else {
            stream = fs.createReadStream(installPackage);
          }
          return extractStream(stream, obj.tmpdir).then(() => obj);
        })
        .then(obj => {
          const { name, version } = require(path.join(
            obj.tmpdir,
            'package.json'
          ));
          obj.cleanup();
          return { name, version };
        })
        .catch(err => {
          // The package name could be with or without semver version, e.g. react-scripts-0.2.0-alpha.1.tgz
          // However, this function returns package name only without semver version.
          console.log(
            `Could not extract the package name from the archive: ${err.message}`
          );
          const assumedProjectName = installPackage.match(
            /^.+\/(.+?)(?:-\d+.+)?\.(tgz|tar\.gz)$/
          )[1];
          console.log(
            `Based on the filename, assuming it is "${chalk.cyan(
              assumedProjectName
            )}"`
          );
          return Promise.resolve({ name: assumedProjectName });
        });
    } else if (installPackage.startsWith('git+')) {
      // Pull package name out of git urls e.g:
      // git+https://github.com/mycompany/react-scripts.git
      // git+ssh://github.com/mycompany/react-scripts.git#v1.2.3
      return Promise.resolve({
        name: installPackage.match(/([^/]+)\.git(#.*)?$/)[1],
      });
    } else if (installPackage.match(/.+@/)) {
      // Do not match @scope/ when stripping off @version or @tag
      return Promise.resolve({
        name: installPackage.charAt(0) + installPackage.substr(1).split('@')[0],
        version: installPackage.split('@')[1],
      });
    } else if (installPackage.match(/^file:/)) {
      const installPackagePath = installPackage.match(/^file:(.*)?$/)[1];
      const { name, version } = require(path.join(
        installPackagePath,
        'package.json'
      ));
      return Promise.resolve({ name, version });
    }
    return Promise.resolve({ name: installPackage });
  },

  checkNodeVersion: function(packageName) {
    const packageJsonPath = path.resolve(
      process.cwd(),
      'node_modules',
      packageName,
      'package.json'
    );

    if (!fs.existsSync(packageJsonPath)) {
      return;
    }

    const packageJson = require(packageJsonPath);
    if (!packageJson.engines || !packageJson.engines.node) {
      return;
    }

    if (!semver.satisfies(process.version, packageJson.engines.node)) {
      console.error(
        chalk.red(
          'You are running Node %s.\n' +
            'Create React App requires Node %s or higher. \n' +
            'Please update your version of Node.'
        ),
        process.version,
        packageJson.engines.node
      );
      process.exit(1);
    }
  },

  checkAppName: function(appName) {
    const validationResult = validateProjectName(appName);
    if (!validationResult.validForNewPackages) {
      console.error(
        chalk.red(
          `Cannot create a project named ${chalk.green(
            `"${appName}"`
          )} because of npm naming restrictions:\n`
        )
      );
      [
        ...(validationResult.errors || []),
        ...(validationResult.warnings || []),
      ].forEach(error => {
        console.error(chalk.red(`  * ${error}`));
      });
      console.error(chalk.red('\nPlease choose a different project name.'));
      process.exit(1);
    }

    // const dependencies = ['react', 'react-dom', 'react-scripts'].sort();
    // if (dependencies.includes(appName)) {
    //   console.error(
    //     chalk.red(
    //       `Cannot create a project named ${chalk.green(
    //         `"${appName}"`
    //       )} because a dependency with the same name exists.\n` +
    //         `Due to the way npm works, the following names are not allowed:\n\n`
    //     ) +
    //       chalk.cyan(dependencies.map(depName => `  ${depName}`).join('\n')) +
    //       chalk.red('\n\nPlease choose a different project name.')
    //   );
    //   process.exit(1);
    // }
  },

  checkIfOnline: function(useYarn) {
    if (!useYarn) {
      // Don't ping the Yarn registry.
      // We'll just assume the best case.
      return Promise.resolve(true);
    }

    return new Promise(resolve => {
      dns.lookup('registry.yarnpkg.com', err => {
        let proxy;
        if (err != null && (proxy = getProxy())) {
          // If a proxy is defined, we likely can't resolve external hostnames.
          // Try to resolve the proxy name as an indication of a connection.
          dns.lookup(url.parse(proxy).hostname, proxyErr => {
            resolve(proxyErr == null);
          });
        } else {
          resolve(err == null);
        }
      });
    });
  },
  // If project only contains files generated by GH, it’s safe.
  // Also, if project contains remnant error logs from a previous
  // installation, lets remove them now.
  isSafeToCreateProjectIn: function(root, name) {
    const validFiles = [
      '.DS_Store',
      '.git',
      '.gitattributes',
      '.gitignore',
      '.gitlab-ci.yml',
      '.hg',
      '.hgcheck',
      '.hgignore',
      '.idea',
      '.npmignore',
      '.travis.yml',
      'docs',
      'LICENSE',
      'README.md',
      'mkdocs.yml',
      'Thumbs.db',
    ];
    // These files should be allowed to remain on a failed install, but then
    // silently removed during the next create.
    const errorLogFilePatterns = [
      'npm-debug.log',
      'yarn-error.log',
      'yarn-debug.log',
    ];
    const isErrorLog = file => {
      return errorLogFilePatterns.some(pattern => file.startsWith(pattern));
    };

    const conflicts = fs
      .readdirSync(root)
      .filter(file => !validFiles.includes(file))
      // IntelliJ IDEA creates module files before CRA is launched
      .filter(file => !/\.iml$/.test(file))
      // Don't treat log files from previous installation as conflicts
      .filter(file => !isErrorLog(file));

    if (conflicts.length > 0) {
      console.log(
        `The directory ${chalk.green(name)} contains files that could conflict:`
      );
      console.log();
      for (const file of conflicts) {
        try {
          const stats = fs.lstatSync(path.join(root, file));
          if (stats.isDirectory()) {
            console.log(`  ${chalk.blue(`${file}/`)}`);
          } else {
            console.log(`  ${file}`);
          }
        } catch (e) {
          console.log(`  ${file}`);
        }
      }
      console.log();
      console.log(
        'Either try using a new directory name, or remove the files listed above.'
      );

      return false;
    }

    // Remove any log files from a previous installation.
    fs.readdirSync(root).forEach(file => {
      if (isErrorLog(file)) {
        fs.removeSync(path.join(root, file));
      }
    });
    return true;
  },

  // See https://github.com/facebook/create-react-app/pull/3355
  checkThatNpmCanReadCwd: function() {
    const cwd = process.cwd();
    let childOutput = null;
    try {
      // Note: intentionally using spawn over exec since
      // the problem doesn't reproduce otherwise.
      // `npm config list` is the only reliable way I could find
      // to reproduce the wrong path. Just printing process.cwd()
      // in a Node process was not enough.
      childOutput = spawn.sync('npm', ['config', 'list']).output.join('');
    } catch (err) {
      // Something went wrong spawning node.
      // Not great, but it means we can't do this check.
      // We might fail later on, but let's continue.
      return true;
    }
    if (typeof childOutput !== 'string') {
      return true;
    }
    const lines = childOutput.split('\n');
    // `npm config list` output includes the following line:
    // "; cwd = C:\path\to\current\dir" (unquoted)
    // I couldn't find an easier way to get it.
    const prefix = '; cwd = ';
    const line = lines.find(line => line.startsWith(prefix));
    if (typeof line !== 'string') {
      // Fail gracefully. They could remove it.
      return true;
    }
    const npmCWD = line.substring(prefix.length);
    if (npmCWD === cwd) {
      return true;
    }
    console.error(
      chalk.red(
        `Could not start an npm process in the right directory.\n\n` +
          `The current directory is: ${chalk.bold(cwd)}\n` +
          `However, a newly started npm process runs in: ${chalk.bold(
            npmCWD
          )}\n\n` +
          `This is probably caused by a misconfigured system terminal shell.`
      )
    );
    if (process.platform === 'win32') {
      console.error(
        chalk.red(`On Windows, this can usually be fixed by running:\n\n`) +
          `  ${chalk.cyan(
            'reg'
          )} delete "HKCU\\Software\\Microsoft\\Command Processor" /v AutoRun /f\n` +
          `  ${chalk.cyan(
            'reg'
          )} delete "HKLM\\Software\\Microsoft\\Command Processor" /v AutoRun /f\n\n` +
          chalk.red(`Try to run the above two lines in the terminal.\n`) +
          chalk.red(
            `To learn more about this problem, read: https://blogs.msdn.microsoft.com/oldnewthing/20071121-00/?p=24433/`
          )
      );
    }
    return false;
  },

  makeCaretRange: function(dependencies, name) {
    const version = dependencies[name];

    if (typeof version === 'undefined') {
      console.error(chalk.red(`Missing ${name} dependency in package.json`));
      process.exit(1);
    }

    let patchedVersion = `^${version}`;

    if (!semver.validRange(patchedVersion)) {
      console.error(
        `Unable to patch ${name} dependency version because version ${chalk.red(
          version
        )} will become invalid ${chalk.red(patchedVersion)}`
      );
      patchedVersion = version;
    }

    dependencies[name] = patchedVersion;
  },

  setCaretRangeForRuntimeDeps: function(packageName) {
    const packagePath = path.join(process.cwd(), 'package.json');
    const packageJson = require(packagePath);

    if (typeof packageJson.dependencies === 'undefined') {
      console.error(chalk.red('Missing dependencies in package.json'));
      process.exit(1);
    }

    const packageVersion = packageJson.dependencies[packageName];
    if (typeof packageVersion === 'undefined') {
      console.error(chalk.red(`Unable to find ${packageName} in package.json`));
      process.exit(1);
    }

    fs.writeFileSync(packagePath, JSON.stringify(packageJson, null, 2) + os.EOL);
  },

  isInGitRepository: function() {
    try {
      execSync('git rev-parse --is-inside-work-tree', { stdio: 'ignore' });
      return true;
    } catch (e) {
      return false;
    }
  },

  isInMercurialRepository: function() {
    try {
      execSync('hg --cwd . root', { stdio: 'ignore' });
      return true;
    } catch (e) {
      return false;
    }
  },

  tryGitInit: function() {
    try {
      execSync('git --version', { stdio: 'ignore' });
      if (this.isInGitRepository() || this.isInMercurialRepository()) {
        return false;
      }

      execSync('git init', { stdio: 'ignore' });
      return true;
    } catch (e) {
      console.warn('Git repo not initialized', e);
      return false;
    }
  },

  tryGitCommit: function(appPath) {
    try {
      execSync('git add -A', { stdio: 'ignore' });
      execSync('git commit -m "Initialize project using Create React App"', {
        stdio: 'ignore',
      });
      return true;
    } catch (e) {
      // We couldn't commit in already initialized git repo,
      // maybe the commit author config is not set.
      // In the future, we might supply our own committer
      // like Ember CLI does, but for now, let's just
      // remove the Git files to avoid a half-done state.
      console.warn('Git commit not created', e);
      console.warn('Removing .git directory...');
      try {
        // unlinkSync() doesn't work on directories.
        fs.removeSync(path.join(appPath, '.git'));
      } catch (removeErr) {
        // Ignore.
      }
      return false;
    }
  }
}